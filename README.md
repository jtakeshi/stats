A small simple stats script. Originally written for calculating statistics on performance results of research software.

Each line should be in the format:
label value
label is optional, and can be any string. value should be interpretable as floating point. Unlabeled values will be grouped into a single category.

Lines beginning with a '#' will be treated as comments, i.e. ignored.
